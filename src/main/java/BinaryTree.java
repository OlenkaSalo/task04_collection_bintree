

import java.util.*;

public class BinaryTree<K, V> {
    Scanner sc = new Scanner(System.in);
    Node<K,V> root = new Node<K, V>(sc.nextInt());

    public V get(K key)
    {
        if(root!=null)
        {
            return root.get(key);
        }
        else
            return null;

    }
    public V put(K key, V value)
    {
        V Value = root.get(key);
        if(root!=null)
        {
            return root.put(key,value);
        }else
        {
            return null;
        }
    }
    public V remove(K key)
    {
        if(root!=null)
        {
            return root.remove(key);
        }else
            return null;

    }




}
