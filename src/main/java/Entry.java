public class Entry<K,V> {
     K key;
     V value;

    public Entry(final K key, final V value) {
        this.key = key;
        this.value = value;
    }




    public K getKey() {
        return key;
    }


    public V getValue() {
        return value;
    }


    public V setValue(final V value) {
        final V old = this.value;
        this.value = value;
        return old;
    }

}


