import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Node<K,V> {


    private Node<K,V> left ;
    private Node<K,V> right;
   private int idhash;
   List<Entry> ent = new LinkedList<>();

    public Node(int idhash)
    {
        this.idhash=idhash;
    }


    public V get(K key)
    {
        int idKey=key.hashCode();
        if(idKey==idhash)
        {
            Iterator<Entry> it = ent.iterator();

            while (it.hasNext()) {
                final Entry<K, V> e = it.next();
                if (e.key.equals(key)) {
                  return e.value;
                }
            }
           return null;
        }
        else if(idKey<idhash)
        {
            if(left!=null)
            {
                return left.get(key);
            }else return null;
        }else{
            if(right!=null)
            {
                return right.get(key);
            }else return null;
        }
    }

     public V put(K key, V value)
     {
         V Value = null;
         int idKey=key.hashCode();
         if(idKey==idhash)
         {
              Iterator<Entry> it = ent.iterator();
             while (it.hasNext()) {
                 final Entry<K, V> e = it.next();
                 if (e.key.equals(key)) {
                     Value = e.value;
                     it.remove();
                 }
             }
             ent.add(new Entry<>(key, value));
         }
         else if(idKey<idhash)
         {
             if(left!=null)
             {
                 Value=left.put(key,value);
             }
             else {
                 left = createNode(key, value);
                 Value=null;
             }
         }
         else if(idKey> idhash)
         {
             if(right!=null)
             {
                 Value=right.put(key,value);
             }
             else {
                 right = createNode(key, value);
                 Value=null;
             }

         }
     return Value;
     }

    public V remove(K key) {
         int idKey = key.hashCode();
        if (idKey==idhash) {

                    return null;

        } else if (idKey<idKey) {
            if (left!=null) {
                return left.remove(key);
            } else {
                return null;
            }
        } else {
            if (right!=null) {
                return right.remove(key);
            } else {
                return null;
            }
        }
    }
     public  static <K,V> Node<K, V> createNode(K key, V value) {
         Node<K, V> node = new Node<>(key.hashCode());
        node.ent.add(new Entry<>(key, value));
        return node;
    }


}
